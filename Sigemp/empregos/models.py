#!/usr/local/bin/python
# -*- coding: utf-8 -*-
from django.db import models

class Usuario(models.Model):
    class Meta:
        verbose_name_plural = "Usuários"
    senha = models.CharField('Senha', max_length=100)
    nome = models.CharField(max_length=100)
    login = models.CharField('Login', max_length=50)
    
    
class Candidato(Usuario):
    dataNascimento = models.DateField('Data de Nascimento')
    email = models.EmailField('E-mail')
    sexo = models.CharField('Sexo', max_length=10)
    
class AreaFormacao(models.Model):
    class Meta:
        verbose_name_plural = "Áreas de Formação"
    candidato = models.ForeignKey(Candidato) 
    descricao = models.CharField('Descrição', max_length=100)  
    
class Empresa (Usuario):
    areaDeTrabalho = models.CharField('Área de Trabalho', max_length=100)
    email = models.EmailField('E-mail')
    razaoSocial = models.CharField('Razão Social', max_length=255)
    dataCriacao = models.DateField('Data de Criação')
    
class Vaga (models.Model):
    empresa = models.ForeignKey(Empresa)
    descricao = models.CharField ('Descrição', max_length=255)
    perfilDesejado = models.CharField('Perfil Desejado', max_length=255)
    duracao = models.IntegerField('Duração')
    
class ProcessoSeletivo(models.Model):
    class Meta:
        verbose_name_plural = "Processos Seletivos"
    empresa = models.ForeignKey(Empresa)
    cargo = models.CharField ('cargo', max_length=100)
    
class RegistroInteresse(models.Model):
    class Meta:
        verbose_name_plural = "Registros de Interesse"
    candidato = models.ForeignKey(Candidato)
    vaga = models.ForeignKey(Vaga)
    
class Inscricao(models.Model):
    class Meta:
        verbose_name_plural = "Inscrições"
    candidato = models.ForeignKey(Candidato)
    processoSeletivo = models.ForeignKey(ProcessoSeletivo)


