from django.contrib import admin
from models import *

admin.site.register(Usuario)
admin.site.register(Candidato)
admin.site.register(Empresa)
admin.site.register(Vaga)
admin.site.register(RegistroInteresse)
admin.site.register(AreaFormacao)
admin.site.register(ProcessoSeletivo)
admin.site.register(Inscricao)
